import logging
import sqlite3
from contextlib import closing
from email.utils import parsedate_to_datetime
from pathlib import Path

import requests

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
logger.propagate = False
log_path = Path(__file__).parent / "app.log"
f_handler = logging.FileHandler(str(log_path))
f_handler.setLevel(logging.INFO)
f_format = logging.Formatter("%(asctime)s [%(levelname)s] %(filename)s: %(message)s")
f_handler.setFormatter(f_format)
logger.addHandler(f_handler)


def collect_system_jumps(cur):
    cur.execute(
        """
            CREATE TABLE IF NOT EXISTS system_jumps (
                id INTEGER PRIMARY KEY,
                ship_jumps INTEGER NOT NULL,
                system_id INTEGER NOT NULL,
                timestamp TIMESTAMP NOT NULL
            );
        """
    )
    cur.execute("CREATE INDEX IF NOT EXISTS system_id_idx ON system_jumps (system_id);")
    logger.info("Fetching system jumps from ESI...")
    r = requests.get(
        "https://esi.evetech.net/latest/universe/system_jumps/",
        headers={"accept": "application/json", "Cache-Control": "no-cache"},
    )
    r.raise_for_status()
    timestamp = parsedate_to_datetime(r.headers["Last-Modified"])
    data = [(row["ship_jumps"], row["system_id"], timestamp) for row in r.json()]
    cur.executemany(
        """INSERT INTO system_jumps (ship_jumps, system_id, timestamp)
        VALUES (?, ?, ?);""",
        data,
    )
    cur.connection.commit()
    logger.info(f"system jumps: Added {len(data)} rows in the DB.")


def collect_system_kills(cur):
    cur.execute(
        """
            CREATE TABLE IF NOT EXISTS system_kills (
                id INTEGER PRIMARY KEY,
                npc_kills INTEGER NOT NULL,
                pod_kills INTEGER NOT NULL,
                ship_kills INTEGER NOT NULL,
                system_id INTEGER NOT NULL,
                timestamp TIMESTAMP NOT NULL
            );
        """
    )
    cur.execute("CREATE INDEX IF NOT EXISTS system_id_idx ON system_kills (system_id);")
    logger.info("Fetching system kills from ESI...")
    r = requests.get(
        "https://esi.evetech.net/latest/universe/system_kills/",
        headers={"accept": "application/json", "Cache-Control": "no-cache"},
    )
    r.raise_for_status()
    timestamp = parsedate_to_datetime(r.headers["Last-Modified"])
    data = [
        (
            row["npc_kills"],
            row["pod_kills"],
            row["ship_kills"],
            row["system_id"],
            timestamp,
        )
        for row in r.json()
    ]
    cur.executemany(
        """INSERT INTO system_kills (
            npc_kills, pod_kills, ship_kills, system_id, timestamp
        )
        VALUES (?, ?, ?, ?, ?)""",
        data,
    )
    cur.connection.commit()
    logger.info(f"system kills: Added {len(data):,} rows in the DB.")


def main():
    logger.info(f"Running with SQLite runtime {sqlite3.sqlite_version}")
    try:
        db_path = Path(__file__).parent / "data.db"
        with closing(
            sqlite3.connect(
                str(db_path),
                detect_types=sqlite3.PARSE_DECLTYPES | sqlite3.PARSE_COLNAMES,
            )
        ) as con:
            with closing(con.cursor()) as cur:
                collect_system_jumps(cur)
                collect_system_kills(cur)
    except Exception:
        logger.exception("An unexpected error occured")
    logger.info("DONE.")


if __name__ == "__main__":
    main()
